﻿CREATE VIEW [dbo].AllSensors
	AS SELECT s.[Id]
      ,s.[Name]
      ,s.[Price]
      ,s.[Amount]
      ,s.[Description]
      ,s.[Properties]
	  ,c.DisplayName as CategoryName
	  ,c.Id as CategoryId
	  ,p.Name as ProviderName
	  ,p.Id as ProviderId
from dbo.Sensors s
join Categories c on s.CategoryId = c.Id
join Providers p on s.ProviderId = p.Id
