﻿/*CREATE ROLE TableAccess
GRANT SELECT,INSERT,UPDATE,DELETE  ON dbo.Sensors TO TableAccess;
GRANT SELECT,INSERT,UPDATE,DELETE  ON dbo.GEOSTATE TO TableAccess;

--now test the role
CREATE USER User1 WITHOUT LOGIN;

EXEC sp_addrolemember  'TableAccess','User1'*/

-- Images
DELETE FROM dbo.Images;
-- Sensors
DELETE FROM dbo.Sensors;
-- Providers
DELETE FROM dbo.Providers;
DBCC CHECKIDENT ('Providers', RESEED, 0)
-- Category Properties
DELETE FROM dbo.CategoryProperties;
-- Categories
DELETE FROM dbo.Categories;
DBCC CHECKIDENT ('Categories', RESEED, 0)


-- Categories
INSERT INTO [dbo].[Categories]
           ([DisplayName],
		   [ShortName])
     VALUES
           ('Magnetic field', 'magnetic'),
		   ('Current', 'current'),
		   ('Temperature', 'temperature'),
		   ('Pressure', 'pressure')



-- Category Properties


INSERT INTO [dbo].[CategoryProperties]
           ([CategoryId]
           ,[Name]
		   ,[Searchable])
     VALUES
           (1, 'Output signal type', 1),
           (1, 'Sensing element type', 0),
           (1, 'Presence of built-in magnet', 0),
           (1, 'Type of sensitivity to the field', 0),
           (1, 'Rise time, μs', 0),
           (1, 'Min supply voltage, V', 0),
           (1, 'Max supply voltage, V', 0),
           (1, 'Max output current, mA', 0),
           (1, 'Temperature range, C°', 0),
           (1, 'Housing', 0),

           (3, 'Min measured temperature, C°', 0),
           (3, 'Max measured temperature, C°', 0),
           (3, 'Sensing element type', 0),
           (3, 'Accuracy, %', 0),
           (3, 'Response time, s', 0),
           (3, 'Measured medium', 0),
           (3, 'Output signal', 0),

           (4, 'Max working pressure, kPa', 0),
           (4, 'Output voltage at maximum pressure, V', 0),
           (4, 'Accuracy, % of max', 0),
           (4, 'Thermocompensation', 0),
           (4, 'Supply voltage, V', 0),
           (4, 'Measured environment', 0),
           (4, 'Sensor type', 0),
           (4, 'Output interface type', 0),
           (4, 'Operating temperature range, C°', 0),
           (4, 'Maximum allowed pressure, Pa', 0),
           (4, 'Reaction time, ms', 0),
           (4, 'Housing', 0)



-- Providers
INSERT INTO [dbo].Providers
           ([Name])
     VALUES
           ('Honeywell'),
		   ('Analog devices'),
		   ('Maxim')



-- Sensors
/*INSERT INTO [dbo].[Sensors]
           ([Id]
           ,[Name]
           ,[CategoryId]
           ,[Price]
           ,[ProviderId]
           ,[Amount]
           ,[Description]
           ,[Properties])
     VALUES
           ('1GT101DC'
           ,'Датчик Холла герметичный ABS'
           ,1
           ,5540
           ,1
           ,25
           ,'<p>Sealed in probe-style package for physical protection and cost-effective installation. Sensor electronically selfadjusts to slight variations in runout and temperature, often simplifying installation and maintenance. Circuit senses movement of targets in camshaft and crankshaft speed and position, transmission speed, and tachometer applications, as well as anti-skid and traction control applications.</p>
<p>• Senses ferrous metal targets<br>• Digital current sinking output (open collector)<br>• Better signal-to-noise ratio than variable reluctance sensors<br>• Excellent low speed performance<br>• Output amplitude not dependant on RPM<br>• Sensor electronically self adjusts to slight variations in runout and variation in temperature<br>• Fast operating speed - over 100 kHz<br>• EMI resistant<br>• Reverse polarity protection and transient protection (integrated into Hall IC)<br>• Wide continuous operating temperature range</p>'
           ,'[{"name": "Output signal type", "value": "цифровой с ок"},
		   {"name": "Sensing element type", "value": "элемент холла"},
		   {"name": "Presence of built-in magnet", "value": "есть"},
		   {"name": "Type of sensitivity to the field", "value": "встроенный магнит"},
		   {"name": "Rise time", "value": "15"},
		   {"name": "Min supply voltage", "value": "4.5"},
		   {"name": "Max supply voltage", "value": "24"},
		   {"name": "Max output current", "value": "20"},
		   {"name": "Temperature range", "value": "-40…150"}],
		   {"name": "Housing", "value": "1gt1"}]')
		   */