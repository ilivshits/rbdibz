﻿CREATE TABLE [dbo].[Sensors]
(
	[Id] NVARCHAR(50) NOT NULL PRIMARY KEY CONSTRAINT SENSOR_IDENTITY_CK CHECK(LEN(Id) > 6), 
    [Name] NVARCHAR(50) NOT NULL, 
	[CategoryId] INT NOT NULL,
    [Price] MONEY NULL, 
    [ProviderId] INT NULL,
	[Amount] INT NULL,
	[Description] NVARCHAR(MAX) NULL,
	[Properties] xml NULL,
	
	FOREIGN KEY ([CategoryId]) REFERENCES Categories([Id]),
	FOREIGN KEY ([ProviderId]) REFERENCES Providers(Id),
)
