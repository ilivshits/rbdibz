﻿CREATE TABLE [dbo].[Images]
(
	[Path] NVARCHAR(450) NOT NULL,
	[SensorId] NVARCHAR(50) NOT NULL,

	FOREIGN KEY ([SensorId]) REFERENCES Sensors(Id),
    CONSTRAINT [PK_Images] PRIMARY KEY ([SensorId], [Path])
)
