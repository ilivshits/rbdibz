﻿CREATE TABLE [dbo].[CategoryProperties]
(
	[CategoryId] INT NOT NULL,
	[Name] NVARCHAR(256) NOT NULL, 
    [Searchable] BIT NOT NULL DEFAULT 0, 

    CONSTRAINT [PK_CategoryProperties] PRIMARY KEY ([CategoryId], [Name]),
	FOREIGN KEY ([CategoryId]) REFERENCES Categories(Id),
)
