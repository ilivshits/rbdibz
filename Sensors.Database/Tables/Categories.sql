﻿CREATE TABLE [dbo].[Categories]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DisplayName] NVARCHAR(50) NOT NULL,
    [ShortName] NVARCHAR(15) NOT NULL
)
