import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../Shared/_services/category.service";
import {Category} from "../../Shared/_models/category.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  moduleId: module.id,
  templateUrl: 'categories.component.html',
  styleUrls: ['categories.component.scss']
})

export class CategoriesComponent implements OnInit {

  public categories: Array<Category>;

  constructor(private _categoryService: CategoryService, private route: ActivatedRoute) {}

  ngOnInit() {
    this._categoryService.getCategories().subscribe(cats => {
      this.categories = cats;
    })
  }
}
