import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {CategoriesComponent} from "./Categories/categories.component";
import {SensorListComponent} from "./SensorsList/sensor-list.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    CategoriesComponent,
    SensorListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule
  ],
  providers: []
})
export class CatalogModule { }
