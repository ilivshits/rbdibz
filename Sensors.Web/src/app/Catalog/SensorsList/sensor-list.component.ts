import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../Shared/_services/category.service";
import {Category} from "../../Shared/_models/category.model";

@Component({
  moduleId: module.id,
  templateUrl: 'sensor-list.component.html',
  styleUrls: ['sensor-list.component.scss']
})

export class SensorListComponent implements OnInit {

  public categories: Array<Category>;

  constructor(private _categoryService: CategoryService) {}

  ngOnInit() {
    this._categoryService.getCategories().subscribe(cats => {
      this.categories = cats;
    })
  }
}
