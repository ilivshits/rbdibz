import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './Header/header.component';
import { NavigationComponent } from './Header/navigation/navigation.component';
import {CategoryService} from "./_services/category.service";

@NgModule({
  declarations: [
    HeaderComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule
  ],
  exports: [
    HeaderComponent
  ],
  providers: [
    CategoryService
  ]
})
export class SharedModule { }
