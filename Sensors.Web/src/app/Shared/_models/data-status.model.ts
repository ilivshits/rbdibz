export class DataStatus {
  tableName: string;
  indexName: string;
  rows: number;
  totalPages: number;
  usedPages: number;
  dataPages: number;
}
