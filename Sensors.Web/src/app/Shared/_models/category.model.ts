export class Category {
  id: number;
  displayName: string;
  shortName: string;
  count: number;
}
