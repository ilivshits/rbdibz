import {Category} from "./category.model";
import {Provider} from "./provider.model";
export class Sensor {
  id: string;
  name: string;
  category: Category;
  price: number;
  provider: Provider;
  amount: number;
  description: string;
  properties: any;
}
