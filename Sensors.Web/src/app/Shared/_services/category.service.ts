import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {Category} from "../_models/category.model";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CategoryService {

  constructor(private _http: HttpClient) { }

  getCategories(): Observable<Array<Category>> {
    return this._http.get<Array<Category>>('http://localhost/Sensors.Service/api/category');
  }

  getCategory(id: number): Observable<Category> {
    return this._http.get<Category>(`http://localhost/Sensors.Service/api/category/${id}`);
  }

  updateCategory(category: any): Observable<any> {
    return this._http.put(`http://localhost/Sensors.Service/api/category`, category);
  }

  addCategory(category: any): Observable<any> {
    return this._http.post(`http://localhost/Sensors.Service/api/category`, category);
  }

  deleteCategory(id: number): Observable<any> {
    return this._http.delete(`http://localhost/Sensors.Service/api/category/${id}`);
  }
}
