import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {HttpClient} from "@angular/common/http";
import {Provider} from "../_models/provider.model";

@Injectable()
export class ProviderService {

  constructor(private _http: HttpClient) { }

  getProviders(): Observable<Array<Provider>> {
    return this._http.get<Array<Provider>>('http://localhost/Sensors.Service/api/provider');
  }

  getProvider(id: number): Observable<Provider> {
    return this._http.get<Provider>(`http://localhost/Sensors.Service/api/provider/${id}`);
  }

  updateProvider(provider: any): Observable<any> {
    return this._http.put(`http://localhost/Sensors.Service/api/provider`, provider);
  }

  addProvider(provider: any): Observable<any> {
    return this._http.post(`http://localhost/Sensors.Service/api/provider`, provider);
  }

  deleteProvider(id: number): Observable<any> {
    return this._http.delete(`http://localhost/Sensors.Service/api/provider/${id}`);
  }
}
