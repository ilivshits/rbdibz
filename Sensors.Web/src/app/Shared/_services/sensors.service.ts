import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {HttpClient} from "@angular/common/http";
import {Sensor} from "../_models/sensor.model";

@Injectable()
export class SensorsService {

  constructor(private _http: HttpClient) { }

  getSensors(): Observable<Array<Sensor>> {
    return this._http.get<Array<Sensor>>('http://localhost/Sensors.Service/api/sensor');
  }

  getSensor(id: number): Observable<Sensor> {
    return this._http.get<Sensor>(`http://localhost/Sensors.Service/api/sensor/${id}`);
  }

  updateSensor(sensor: any): Observable<any> {
    return this._http.put(`http://localhost/Sensors.Service/api/sensor`, sensor);
  }

  addSensor(sensor: any): Observable<any> {
    return this._http.post(`http://localhost/Sensors.Service/api/sensor`, sensor);
  }

  deleteSensor(id: number): Observable<any> {
    return this._http.delete(`http://localhost/Sensors.Service/api/sensor/${id}`);
  }
}
