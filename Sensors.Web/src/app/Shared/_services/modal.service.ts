import * as _ from 'underscore';
import {Injectable, ElementRef} from "@angular/core";

@Injectable()
export class ModalService {
  private modals: any[] = [];

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    let modalToRemove = _.findWhere(this.modals, { id: id });
    this.modals = _.without(this.modals, modalToRemove);
  }

  open(id: string, data: any = {}) {
    // open modal specified by id
    let modal = _.findWhere(this.modals, { id: id });

    for (let param in data) {
      let els = modal.el.nativeElement.querySelectorAll(`.${param}`);
      for (let el of els) {
        if (el != null) {
          if (el.tagName == 'INPUT') {
            el.value = data[param];
            el.dispatchEvent(new Event('input'));
          } else if (el.tagName == 'SELECT') {
            el.innerHTML = data[param].reduce((previousValue, currentValue, index, array) => {
              return previousValue + `<option ${currentValue.selected ? "selected" : ""} value="${currentValue.value}">${currentValue.displayName}</option>`;
            }, '');
            el.dispatchEvent(new Event('change'));
          } else {
            el.innerHTML = data[param];
          }
        }
      }
    }
    modal.open();
  }

  close(id: string) {
    // close modal specified by id
    let modal = _.find(this.modals, { id: id });
    modal.close();
  }
}
