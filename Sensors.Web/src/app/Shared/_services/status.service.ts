import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {HttpClient} from "@angular/common/http";
import {Sensor} from "../_models/sensor.model";
import {DataStatus} from "../_models/data-status.model";

@Injectable()
export class StatusService {

  constructor(private _http: HttpClient) { }

  getStatus(): Observable<Array<DataStatus>> {
    return this._http.get<Array<DataStatus>>('http://localhost/Sensors.Service/api/status/data');
  }
}
