import { Component, OnInit } from '@angular/core';
import {NgForm, FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import {ProviderService} from "../../../Shared/_services/provider.service";

@Component({
  moduleId: module.id,
  selector: 'provider-add-modal',
  templateUrl: 'provider-add-form.component.html'
})

export class ProviderAddFormComponent implements OnInit {

  addProviderForm: FormGroup;

  constructor(private fb: FormBuilder, private _providerService: ProviderService) {}

  public ngOnInit():void {
    this.addProviderForm = new FormGroup({
      name: new FormControl('', [<any>Validators.required, <any>Validators.minLength(6)])
    });
  }

  submit(form: NgForm) {
    this._providerService.addProvider(form.value)
      .subscribe(res => {
        window.location.href = location.href;
      });
  }
}
