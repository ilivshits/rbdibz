import {Component, OnInit} from "@angular/core";
import {ModalService} from "../../Shared/_services/modal.service";
import {Category} from "../../Shared/_models/category.model";
import {ProviderService} from "../../Shared/_services/provider.service";
import {Provider} from "../../Shared/_models/provider.model";

@Component({
  moduleId: module.id,
  templateUrl: 'providers.component.html',
  styleUrls: ['providers.component.scss']
})
export class ProvidersComponent implements OnInit {

  public providers: Provider[];

  constructor(
    private _modalService: ModalService,
    private _providerService: ProviderService){ }

  public ngOnInit():void {
    this._providerService.getProviders().subscribe(ps => {
      this.providers = ps;
    })
  }

  editProvider(provider: Provider){
    this._modalService.open('provider-edit-modal', provider);
  }

  addProvider() {
    this._modalService.open('provider-add-modal', {});
  }

  deleteProvider(id: number){
    this._providerService.deleteProvider(id).subscribe(res=>{window.location.href = location.href;});
  }

  closeModal(id: string){
    this._modalService.close(id);
  }
}
