import { Component, OnInit } from '@angular/core';
import {NgForm, FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import {ProviderService} from "../../../Shared/_services/provider.service";

@Component({
  moduleId: module.id,
  selector: 'provider-edit-modal',
  templateUrl: 'provider-edit-form.component.html'
})

export class ProviderEditFormComponent implements OnInit {

  editProviderForm: FormGroup;

  constructor(private fb: FormBuilder, private _providerService: ProviderService) {}

  public ngOnInit():void {
    this.editProviderForm = new FormGroup({
      id: new FormControl('',[<any>Validators.required]),
      name: new FormControl('', [<any>Validators.required, <any>Validators.minLength(6)])
    });
  }

  submit(form: NgForm) {
    this._providerService.updateProvider(form.value)
      .subscribe(res => {
        window.location.href = location.href;
      });
  }
}
