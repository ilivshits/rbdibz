import {Component, OnInit} from "@angular/core";
import {CategoryService} from "../../Shared/_services/category.service";
import {ModalService} from "../../Shared/_services/modal.service";
import {Category} from "../../Shared/_models/category.model";

@Component({
  moduleId: module.id,
  templateUrl: 'category.component.html',
  styleUrls: ['category.component.scss']
})
export class CategoryComponent implements OnInit {

  public categories: Category[];

  constructor(private _categoryService: CategoryService, private _modalService: ModalService){ }

  public ngOnInit():void {
    this._categoryService.getCategories().subscribe(cats => {
      this.categories = cats;
    })
  }

  editCategory(category: Category){
    this._modalService.open('category-edit-modal', category);
  }

  addCategory() {
      this._modalService.open('category-add-modal', {});
  }

  deleteCategory(id: number){
    this._categoryService.deleteCategory(id).subscribe(res=>{window.location.href = location.href;});
  }

  closeModal(id: string){
    this._modalService.close(id);
  }
}
