import { Component, OnInit } from '@angular/core';
import {Category} from "../../../Shared/_models/category.model";
import {CategoryService} from "../../../Shared/_services/category.service";
import {ActivatedRoute} from "@angular/router";
import {NgForm, FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";

@Component({
  moduleId: module.id,
  selector: 'category-edit-modal',
  templateUrl: 'category-edit-form.component.html'
})

export class CategoryEditFormComponent implements OnInit {

  editCategoryForm: FormGroup;

  constructor(private fb: FormBuilder, private _categoryService: CategoryService, private route: ActivatedRoute) {}

  public ngOnInit():void {
    this.editCategoryForm = new FormGroup({
      id: new FormControl('',[<any>Validators.required]),
      displayName: new FormControl('', [<any>Validators.required, <any>Validators.minLength(6)])
    });
  }

  submit(form: NgForm) {
    this._categoryService.updateCategory(form.value)
      .subscribe(res => {
        window.location.href = location.href;
      });
  }
}
