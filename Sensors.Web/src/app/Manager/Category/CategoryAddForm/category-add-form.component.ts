import { Component, OnInit } from '@angular/core';
import {Category} from "../../../Shared/_models/category.model";
import {CategoryService} from "../../../Shared/_services/category.service";
import {NgForm, FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";

@Component({
  moduleId: module.id,
  selector: 'category-add-modal',
  templateUrl: 'category-add-form.component.html'
})

export class CategoryAddFormComponent implements OnInit {

  addCategoryForm: FormGroup;

  constructor(private fb: FormBuilder, private _categoryService: CategoryService) {}

  public ngOnInit():void {
    this.addCategoryForm = new FormGroup({
      displayName: new FormControl('', [<any>Validators.required, <any>Validators.minLength(6)])
    });
  }

  submit(form: NgForm) {
    this._categoryService.addCategory(form.value)
      .subscribe(res => {
        window.location.href = location.href;
      });
  }
}
