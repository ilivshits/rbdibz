import {Component, OnInit} from "@angular/core";
import {CategoryService} from "../../Shared/_services/category.service";
import {ModalService} from "../../Shared/_services/modal.service";
import {Category} from "../../Shared/_models/category.model";
import {Sensor} from "../../Shared/_models/sensor.model";
import {SensorsService} from "../../Shared/_services/sensors.service";
import {ProviderService} from "../../Shared/_services/provider.service";

@Component({
  moduleId: module.id,
  templateUrl: 'sensors.component.html',
  styleUrls: ['sensors.component.scss']
})
export class SensorsComponent implements OnInit {

  public sensors: Sensor[];

  constructor(
    private _sensorsService: SensorsService,
    private _modalService: ModalService,
    private _categoryService: CategoryService,
    private _providerService: ProviderService){ }

  public ngOnInit():void {
    this._sensorsService.getSensors().subscribe(sens => {
      this.sensors = sens;
    })
  }

  editSensor(sensor: Sensor){
    this._categoryService.getCategories().subscribe(categories => {
      this._providerService.getProviders().subscribe(providers => {

        let category = categories.map(c => {
          let cat: any = {value: c.id, displayName: c.displayName};
          if (sensor.category.id == c.id) {
            cat.selected = true;
          }
          return cat;
        });
        let provider = providers.map(p => {
          let pro: any = {value: p.id, displayName: p.name};
          if (sensor.provider.id == p.id) {
            pro.selected = true;
          }
          return pro;
        });

        let obj: any = Object.assign({}, sensor);
        obj.category = category;
        obj.provider = provider;
        this._modalService.open('sensor-edit-modal', obj);
      });
    });
  }

  addSensor() {
    this._categoryService.getCategories().subscribe(categories => {
      this._providerService.getProviders().subscribe(providers => {

        let category = categories.map(c => { return {value: c.id, displayName: c.displayName}});
        let provider = providers.map(c => { return {value: c.id, displayName: c.name}});

        this._modalService.open('sensor-add-modal', {category, provider});
      });
    });
  }

  deleteCategory(id: number){
    this._sensorsService.deleteSensor(id).subscribe(res=>{window.location.href = location.href;});
  }

  closeModal(id: string){
    this._modalService.close(id);
  }
}
