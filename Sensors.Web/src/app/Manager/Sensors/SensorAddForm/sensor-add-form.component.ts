import {Component, OnInit} from '@angular/core';
import {NgForm, FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import {CategoryService} from "../../../Shared/_services/category.service";
import {SensorsService} from "../../../Shared/_services/sensors.service";
import {Sensor} from "../../../Shared/_models/sensor.model";
import {Category} from "../../../Shared/_models/category.model";
import {Provider} from "../../../Shared/_models/provider.model";

@Component({
  moduleId: module.id,
  selector: 'sensor-add-modal',
  templateUrl: 'sensor-add-form.component.html'
})

export class SensorAddFormComponent implements OnInit {

  addSensorForm: FormGroup;

  constructor(private fb: FormBuilder,
              private _sensorService: SensorsService
  ) {}

  public ngOnInit():void {
    this.addSensorForm = new FormGroup({
      name: new FormControl('', [<any>Validators.required]),
      price: new FormControl('', [<any>Validators.required]),
      amount: new FormControl('', [<any>Validators.required]),
      description: new FormControl('', [<any>Validators.required]),
      category: new FormControl('', [<any>Validators.required]),
      provider: new FormControl('', [<any>Validators.required]),
    });
  }

  submit(form: NgForm) {
    let sensor: Sensor = new Sensor();
    sensor.name = form.value.name;
    sensor.amount = form.value.amount;
    sensor.category = new Category();
    sensor.category.id = form.value.category;
    sensor.provider = new Provider();
    sensor.provider.id = form.value.provider;
    sensor.description = form.value.description;
    sensor.price = form.value.price;

    this._sensorService.addSensor(sensor)
      .subscribe(res => {
        window.location.href = location.href;
      });
  }
}
