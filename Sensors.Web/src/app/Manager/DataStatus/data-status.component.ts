import {Component, OnInit} from "@angular/core";
import {StatusService} from "../../Shared/_services/status.service";
import {DataStatus} from "../../Shared/_models/data-status.model";

@Component({
  moduleId: module.id,
  templateUrl: 'data-status.component.html',
  styleUrls: ['data-status.component.scss']
})
export class DataStatusComponent implements OnInit {

  public dataStatuses: DataStatus[];

  constructor(
    private _statusService: StatusService){ }

  public ngOnInit():void {
    this._statusService.getStatus().subscribe(ds => {
      this.dataStatuses = ds;
    })
  }
}
