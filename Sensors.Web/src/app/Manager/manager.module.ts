import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule} from "@angular/router";
import {ManagerComponent} from "./manager.component";
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {CategoryComponent} from "./Category/category.component";
import {CategoryEditFormComponent} from "./Category/CategoryEditForm/category-edit-form.component";
import {ModalService} from "../Shared/_services/modal.service";
import {ModalComponent} from "../Shared/_directives/modal.component";
import {HttpClientModule} from "@angular/common/http";
import {CategoryAddFormComponent} from "./Category/CategoryAddForm/category-add-form.component";
import {SensorsComponent} from "./Sensors/sensors.component";
import {SensorsService} from "../Shared/_services/sensors.service";
import {ProviderService} from "../Shared/_services/provider.service";
import {SensorAddFormComponent} from "./Sensors/SensorAddForm/sensor-add-form.component";
import {ProvidersComponent} from "./Providers/providers.component";
import {ProviderEditFormComponent} from "./Providers/ProviderEditForm/provider-edit-form.component";
import {ProviderAddFormComponent} from "./Providers/ProviderAddForm/provider-add-form.component";
import {SensorEditFormComponent} from "./Sensors/SensorEditForm/sensor-edit-form.component";
import {StatusService} from "../Shared/_services/status.service";
import {DataStatusComponent} from "./DataStatus/data-status.component";

@NgModule({
  declarations: [
    ManagerComponent,

    CategoryComponent,
    CategoryEditFormComponent,
    CategoryAddFormComponent,

    SensorsComponent,
    SensorAddFormComponent,
    SensorEditFormComponent,

    ProvidersComponent,
    ProviderEditFormComponent,
    ProviderAddFormComponent,

    DataStatusComponent,

    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    DataTableModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [ModalService, SensorsService, ProviderService, StatusService]
})
export class ManagerModule { }
