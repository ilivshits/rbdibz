import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {HomeModule} from "./Home/home.module";
import {SharedModule} from "./Shared/shared.module";
import {CatalogModule} from "./Catalog/catalog.module";
import {ManagerModule} from "./Manager/manager.module";
import {ModalService} from "./Shared/_services/modal.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HomeModule,
    SharedModule,
    CatalogModule,
    ManagerModule,
    AppRoutingModule
  ],
  providers: [
    ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
