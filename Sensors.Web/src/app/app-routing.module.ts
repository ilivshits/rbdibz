import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './Home/home.component';
import {CategoriesComponent as CatalogCategoriesComponent} from "./Catalog/Categories/categories.component";
import {CategoryComponent as ManagerCategoriesComponent} from "./Manager/Category/category.component";
import {SensorsComponent as SensorsCategoriesComponent} from "./Manager/Sensors/sensors.component";
import {ProvidersComponent as SensorsProvidersComponent} from "./Manager/Providers/providers.component";
import {SensorListComponent} from "./Catalog/SensorsList/sensor-list.component";
import {ManagerComponent} from "./Manager/manager.component";
import {CategoryEditFormComponent} from "./Manager/Category/CategoryEditForm/category-edit-form.component";
import {DataStatusComponent} from "./Manager/DataStatus/data-status.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'catalog', component: CatalogCategoriesComponent},
  { path: 'catalog/:id', component: SensorListComponent},

  { path: 'manager', component: ManagerComponent},
  { path: 'manager/categories', component: ManagerCategoriesComponent},
  { path: 'manager/sensors', component: SensorsCategoriesComponent},
  { path: 'manager/providers', component: SensorsProvidersComponent},
  { path: 'manager/datastatus', component: DataStatusComponent},

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
