import { Sensors.WebPage } from './app.po';

describe('sensors.web App', () => {
  let page: Sensors.WebPage;

  beforeEach(() => {
    page = new Sensors.WebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
