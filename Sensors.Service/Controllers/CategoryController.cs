﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Sensors.Data;
using Sensors.Web.ViewModels;

namespace Sensors.Service.Controllers
{
    [RoutePrefix("api/category")]
    public class CategoryController : ApiController
    {
        [Route("")]
        public IEnumerable<CategoryViewModel> GetCategories()
        {
            using (var ctx = new SensorsEntities())
            {
                return Mapper.Map<List<CategoryViewModel>>(ctx.Categories.ToList());
            }
        }

        [Route("{id}")]
        public CategoryViewModel GetCategory(int id)
        {
            using (var ctx = new SensorsEntities())
            {
                Category categoryEntity = ctx.Categories.SingleOrDefault(category => category.Id.Equals(id));

                return categoryEntity == null ? null : Mapper.Map<CategoryViewModel>(categoryEntity);
            }
        }

        [HttpPut]
        [Route("")]
        public void Put(CategoryViewModel category)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Categories.Single(cat => cat.Id.Equals(category.Id)).DisplayName = category.DisplayName;
                ctx.SaveChanges();
            }
        }

        [HttpPost]
        [Route("")]
        public void Post(CategoryViewModel category)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Categories.Add(new Category() {DisplayName = category.DisplayName, ShortName = category.DisplayName.ToLower()});
                ctx.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Categories.Remove(ctx.Categories.Single(category => category.Id.Equals(id)));
                ctx.SaveChanges();
            }
        }
    }
}
