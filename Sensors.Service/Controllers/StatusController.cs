﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http;
using Sensors.Data;

namespace Sensors.Service.Controllers
{
    [RoutePrefix("api/status")]
    public class StatusController : ApiController
    {
        [HttpGet]
        [Route("data")]
        public List<getDataStatus_Result> GetDataStatus()
        {
            using (var ctx = new SensorsEntities())
            {
                var isAuthorized = new ObjectParameter("Auth", typeof(bool));

                List<getDataStatus_Result> status = ctx.getDataStatus("admin", "admin", isAuthorized).ToList();
                if (isAuthorized.Value == null)
                {
                    throw new AuthenticationException("Wrong user");
                }

                return status;
            }
        } 
    }
}
