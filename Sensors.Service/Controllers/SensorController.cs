﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Sensors.Data;
using Sensors.Service.Helpers;
using Sensors.Web.ViewModels;

namespace Sensors.Service.Controllers
{
    [RoutePrefix("api/sensor")]
    public class SensorController : ApiController
    {
        [Route("")]
        public IEnumerable<SensorViewModel> GetSensors()
        {
            using (var ctx = new SensorsEntities())
            {
                return Mapper.Map<List<SensorViewModel>>(ctx.AllSensors.ToList());
            }
        }

        [Route("{categoryId}")]
        public IEnumerable<SensorViewModel> GetSensors(int categoryId)
        {
            using (var ctx = new SensorsEntities())
            {
                return Mapper.Map<List<SensorViewModel>>(ctx.Sensors.Where(sensor => sensor.CategoryId.Equals(categoryId)));
            }
        }

        [Route("{id}")]
        public SensorViewModel GetSensor(string id)
        {
            using (var ctx = new SensorsEntities())
            {
                Sensor sensorEntity = ctx.Sensors.SingleOrDefault(sensor => sensor.Id.Equals(id));

                return sensorEntity == null ? null : Mapper.Map<SensorViewModel>(sensorEntity);
            }
        }

        [HttpPut]
        [Route("")]
        public void Put(SensorViewModel sensor)
        {
            using (var ctx = new SensorsEntities())
            {
                Sensor sensorEntity = ctx.Sensors.SingleOrDefault(s => s.Id.Equals(sensor.Id));
                sensorEntity.Name = sensor.Name;
                sensorEntity.Amount = sensor.Amount;
                sensorEntity.CategoryId = sensor.Category.Id;
                sensorEntity.ProviderId = sensor.Provider.Id;
                sensorEntity.Description = sensor.Description;
                sensorEntity.Price = sensor.Price;

                ctx.SaveChanges();
            }
        }

        [HttpPost]
        [Route("")]
        public void Post(SensorViewModel sensor)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Sensors.Add(new Sensor()
                {
                    Id = StringHelper.RandomString(10),
                    Name = sensor.Name,
                    Amount = sensor.Amount,
                    CategoryId = sensor.Category.Id,
                    ProviderId = sensor.Provider.Id,
                    Description = sensor.Description,
                    Price = sensor.Price
                });
                ctx.SaveChanges();
            }
        }
    }
}
