﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Sensors.Data;
using Sensors.Web.ViewModels;

namespace Sensors.Service.Controllers
{
    [RoutePrefix("api/provider")]
    public class ProviderController : ApiController
    {
        [Route("")]
        public IEnumerable<ProviderViewModel> Get()
        {
            using (var ctx = new SensorsEntities())
            {
                return Mapper.Map<List<ProviderViewModel>>(ctx.Providers.ToList());
            }
        }

        [HttpPut]
        [Route("")]
        public void Put(ProviderViewModel provider)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Providers.Single(p => p.Id.Equals(provider.Id)).Name = provider.Name;
                ctx.SaveChanges();
            }
        }

        [HttpPost]
        [Route("")]
        public void Post(ProviderViewModel provider)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Providers.Add(new Provider() { Name = provider.Name });
                ctx.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            using (var ctx = new SensorsEntities())
            {
                ctx.Providers.Remove(ctx.Providers.Single(p => p.Id.Equals(id)));
                ctx.SaveChanges();
            }
        }
    }
}
