﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json.Serialization;

namespace Sensors.Service
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            // Web API routes
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Category",
                routeTemplate: "api/category/{id}",
                defaults: new { controller = "category", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Sensor",
                routeTemplate: "api/sensor/{id}",
                defaults: new { controller = "sensor", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Provider",
                routeTemplate: "api/provider/{id}",
                defaults: new { controller = "provider", id = RouteParameter.Optional }
            );
        }
    }
}
