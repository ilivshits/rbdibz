﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sensors.Web.ViewModels
{
    public class SensorViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public CategoryViewModel Category { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public ProviderViewModel Provider { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }
}