﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sensors.Web.ViewModels
{
    public class ProviderViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}