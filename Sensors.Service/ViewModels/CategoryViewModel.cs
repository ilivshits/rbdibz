﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sensors.Web.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string ShortName { get; set; }
        public int Count { get; set; }
    }
}