﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using AutoMapper;
using Sensors.Data;
using Sensors.Web.ViewModels;

namespace Sensors.Service
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            SetupAutomapper();
        }

        private void SetupAutomapper()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Category, CategoryViewModel>()
                    .ForMember(
                        model => model.Count,
                        opt => opt.MapFrom(category => category.Sensors.Count));

                config.CreateMap<AllSensor, SensorViewModel>()
                    .ForMember(
                        model => model.Category,
                        opt => opt.MapFrom(sensor => new CategoryViewModel()
                        {
                            Id = sensor.CategoryId,
                            DisplayName = sensor.CategoryName
                        }))
                    .ForMember(
                        model => model.Provider,
                        opt => opt.MapFrom(sensor => new ProviderViewModel()
                        {
                            Id = sensor.ProviderId,
                            Name = sensor.ProviderName
                        }));

                config.CreateMap<Provider, ProviderViewModel>();

                config.CreateMap<Sensor, SensorViewModel>()
                    .ForMember(
                        model => model.Category,
                        opt => opt.MapFrom(sensor =>
                            new CategoryViewModel()
                            {
                                Id = sensor.CategoryId,
                                DisplayName = sensor.Category.DisplayName
                            }))
                    .ForMember(
                        model => model.Provider,
                        opt => opt.MapFrom(sensor =>
                            sensor.ProviderId == null
                                ? null
                                : new ProviderViewModel() { Id = sensor.ProviderId.Value, Name = sensor.Provider.Name }));
            });
        }
    }
}
