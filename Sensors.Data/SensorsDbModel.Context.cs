﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sensors.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class SensorsEntities : DbContext
    {
        public SensorsEntities()
            : base("name=SensorsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CategoryProperty> CategoryProperties { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Sensor> Sensors { get; set; }
        public virtual DbSet<AllSensor> AllSensors { get; set; }
        public virtual DbSet<CurrentDataStatu> CurrentDataStatus { get; set; }
    
        public virtual ObjectResult<getAllSensors_Result> getAllSensors()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getAllSensors_Result>("getAllSensors");
        }
    
        public virtual ObjectResult<getDataStatus_Result> getDataStatus(string login, string password, ObjectParameter auth)
        {
            var loginParameter = login != null ?
                new ObjectParameter("Login", login) :
                new ObjectParameter("Login", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getDataStatus_Result>("getDataStatus", loginParameter, passwordParameter, auth);
        }
    
        public virtual ObjectResult<getAllSensors1_Result> getAllSensors1()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getAllSensors1_Result>("getAllSensors1");
        }
    
        public virtual ObjectResult<getSensor_Result> getSensor(Nullable<int> param1, Nullable<int> param2)
        {
            var param1Parameter = param1.HasValue ?
                new ObjectParameter("param1", param1) :
                new ObjectParameter("param1", typeof(int));
    
            var param2Parameter = param2.HasValue ?
                new ObjectParameter("param2", param2) :
                new ObjectParameter("param2", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getSensor_Result>("getSensor", param1Parameter, param2Parameter);
        }
    }
}
