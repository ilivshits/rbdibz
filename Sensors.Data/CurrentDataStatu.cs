//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sensors.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CurrentDataStatu
    {
        public string TableName { get; set; }
        public string indexName { get; set; }
        public Nullable<long> Rows { get; set; }
        public Nullable<long> TotalPages { get; set; }
        public Nullable<long> UsedPages { get; set; }
        public Nullable<long> DataPages { get; set; }
    }
}
