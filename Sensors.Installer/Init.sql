USE [master]
GO
/****** Object:  Database [Sensors]    Script Date: 1/11/2018 0:23:36 ******/
CREATE DATABASE [Sensors]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Sensors', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.ELIVELOCALDB\MSSQL\DATA\Sensors.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Sensors_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.ELIVELOCALDB\MSSQL\DATA\Sensors_log.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Sensors] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Sensors].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Sensors] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [Sensors] SET ANSI_NULLS ON 
GO
ALTER DATABASE [Sensors] SET ANSI_PADDING ON 
GO
ALTER DATABASE [Sensors] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [Sensors] SET ARITHABORT ON 
GO
ALTER DATABASE [Sensors] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Sensors] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Sensors] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Sensors] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Sensors] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [Sensors] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [Sensors] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Sensors] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [Sensors] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Sensors] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Sensors] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Sensors] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Sensors] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Sensors] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Sensors] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Sensors] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Sensors] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Sensors] SET RECOVERY FULL 
GO
ALTER DATABASE [Sensors] SET  MULTI_USER 
GO
ALTER DATABASE [Sensors] SET PAGE_VERIFY NONE  
GO
ALTER DATABASE [Sensors] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Sensors] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Sensors] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Sensors] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Sensors', N'ON'
GO
ALTER DATABASE [Sensors] SET QUERY_STORE = OFF
GO
USE [Sensors]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Sensors]
GO

/****** Object:  Table [dbo].[Providers]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Providers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
	[ShortName] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sensors]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sensors](
	[Id] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Price] [money] NULL,
	[ProviderId] [int] NULL,
	[Amount] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[Properties] [xml] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[AllSensors]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AllSensors]
	AS SELECT s.[Id]
      ,s.[Name]
      ,s.[Price]
      ,s.[Amount]
      ,s.[Description]
      ,s.[Properties]
	  ,c.DisplayName as CategoryName
	  ,c.Id as CategoryId
	  ,p.Name as ProviderName
	  ,p.Id as ProviderId
from dbo.Sensors s
join Categories c on s.CategoryId = c.Id
join Providers p on s.ProviderId = p.Id

GO
/****** Object:  View [dbo].[CurrentDataStatus]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CurrentDataStatus]
	AS SELECT 
    t.NAME AS TableName,
    i.name as indexName,
    p.[Rows],
    sum(a.total_pages) as TotalPages, 
    sum(a.used_pages) as UsedPages, 
    sum(a.data_pages) as DataPages
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
WHERE 
    t.NAME NOT LIKE 'dt%' AND
    i.OBJECT_ID > 255 AND   
    i.index_id <= 1
GROUP BY 
    t.NAME, i.object_id, i.index_id, i.name, p.[Rows]
GO
/****** Object:  Table [dbo].[CategoryProperties]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryProperties](
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Searchable] [bit] NOT NULL,
 CONSTRAINT [PK_CategoryProperties] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Images]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Images](
	[Path] [nvarchar](450) NOT NULL,
	[SensorId] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED 
(
	[SensorId] ASC,
	[Path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CategoryProperties] ADD  DEFAULT ((0)) FOR [Searchable]
GO
ALTER TABLE [dbo].[CategoryProperties]  WITH NOCHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Images]  WITH NOCHECK ADD FOREIGN KEY([SensorId])
REFERENCES [dbo].[Sensors] ([Id])
GO
ALTER TABLE [dbo].[Sensors]  WITH NOCHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Sensors]  WITH NOCHECK ADD FOREIGN KEY([ProviderId])
REFERENCES [dbo].[Providers] ([Id])
GO
ALTER TABLE [dbo].[Sensors]  WITH NOCHECK ADD  CONSTRAINT [SENSOR_IDENTITY_CK] CHECK  ((len([Id])>(6)))
GO
ALTER TABLE [dbo].[Sensors] CHECK CONSTRAINT [SENSOR_IDENTITY_CK]
GO
/****** Object:  StoredProcedure [dbo].[getAllSensors]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAllSensors]
AS
SELECT s.[Id]
      ,s.[Name]
      ,s.[Price]
      ,s.[Amount]
      ,s.[Description]
      ,s.[Properties]
	  ,c.DisplayName as CategoryName
	  ,c.Id as CategoryId
	  ,p.Name as ProviderName
	  ,p.Id as ProviderId
from dbo.Sensors s
join Categories c on s.CategoryId = c.Id
join Providers p on s.ProviderId = p.Id
GO
/****** Object:  StoredProcedure [dbo].[getDataStatus]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getDataStatus]
	@Login nvarchar(max),
	@Password nvarchar(max),
	@Auth bit = 1 OUTPUT
AS 
IF @Login = 'admin' and @Password = 'admin'
begin
	(SELECT 
    t.NAME AS TableName,
    i.name as indexName,
    p.[Rows],
    sum(a.total_pages) as TotalPages, 
    sum(a.used_pages) as UsedPages, 
    sum(a.data_pages) as DataPages
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
WHERE 
    t.NAME NOT LIKE 'dt%' AND
    i.OBJECT_ID > 255 AND   
    i.index_id <= 1
GROUP BY 
    t.NAME, i.object_id, i.index_id, i.name, p.[Rows])
	
end
	ELSE
begin
	set @Auth = 0
end
GO
/****** Object:  StoredProcedure [dbo].[getSensor]    Script Date: 1/11/2018 0:23:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getSensor]
	@param1 int = 0,
	@param2 int
AS
	SELECT @param1, @param2
RETURN 0
GO
USE [master]
GO
ALTER DATABASE [Sensors] SET  READ_WRITE 
GO
