﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sensors.Installer.Helpers
{
    public static class DatabaseHelper
    {
        public static bool IsDatabaseExist(string connString)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
    }
}
