﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sensors.Installer.Helpers;

namespace Sensors.Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DeployBtn_Click(object sender, RoutedEventArgs e)
        {
            LogTb.Document.Blocks.Clear();
            if (!DatabaseHelper.IsDatabaseExist(ConnectionStringTb.Text))
            {
                LogTb.AppendText("Invalid connection string");

                return;
            }

            LogTb.AppendText("Starting deployment...");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringTb.Text))
                {
                    conn.InfoMessage += OnInfoMessage;
                    conn.Open();

                    foreach (string cmdString in ResourcesHelper.GetEmbeddedResource("Init.sql", Assembly.GetExecutingAssembly()).Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        using (SqlCommand cmd = new SqlCommand(cmdString, conn))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }
                }

                HomeView.Visibility = Visibility.Hidden;
                ResultView.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                LogTb.AppendText(Environment.NewLine + Environment.NewLine);
                LogTb.AppendText(ex.Message);
                LogTb.Foreground = Brushes.Black;
            }
        }

        private void OnInfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            LogTb.AppendText(Environment.NewLine + Environment.NewLine);
            if (e.Errors.Count > 0)
            {
                LogTb.Foreground = Brushes.Red;
            }

            LogTb.AppendText(e.Message);
            LogTb.Foreground = Brushes.Black;
        }
    }
}
